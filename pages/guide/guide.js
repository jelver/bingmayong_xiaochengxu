//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    crowd:{},
    list:[],
    order:"",
    sum:0,
    cur:0
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  getCrowdDetail: function(){
    var that = this;
    var list = []
    wx.request({
      url: "https://qcloud.bmy.com.cn/wechat/crowd",
      success: function(data){
        var _data = data.data.data;
        var obj = {};
        var sum = 0;
        var cur = 0;
        var objSort = [];
        for(var i=0;i<_data.length;i++){
          sum += _data[i].realTimeCount.sum;
          cur += _data[i].realTimeCount.current;
          list.push(that.compute(_data[i]));
        }
        list = that.bubbleSort(list);
        for(var i=0;i<_data.length;i++){
          objSort.push("【"+list[i].name+"】");
          objSort = objSort.reverse()
        }
        that.setData({
          crowd:_data,
          list:list,
          order:objSort.join("=>"),
          sum:(sum/10000).toFixed(1),
          cur:(cur/10000).toFixed(1),
          yongjidu: ((sum / 10000)*0.6).toFixed(1)
        })
        wx.hideLoading()
      },
      fail: function () {
        wx.showLoading({
          title: '数据加载错误，请刷新再试！'
        })
      }
    });
  },
  bubbleSort: function(array){
    var i = 0,
        len = array.length,
        j,d;
    for(i=0;i<len;i++){
        for(j=0;j<len;j++){
            if(array[i].scale>array[j].scale){
                d=array[j];
                array[j]=array[i];
                array[i]=d;
            }
        }
    }
    return array;
  },
  compute: function(data){
    var cur = data.realTimeCount.current,
        sum = data.realTimeCount.sum,
        name = data.name;
    if(sum==0){
      return {name:name,cn:"舒适",en:"comfort"};
    }
    var result = (cur/sum).toFixed(0);
    console.log(cur,sum,result);
    if(result<0.6){
      return {name:name,cn:"舒适",en:"comfort",scale:result}
    }else if(result>1){
      return {name:name,cn:"饱和",en:"full",scale:result}
    }else{
      return {name:name,cn:"拥挤",en:"crowd",scale:result}
    }
  },
  onLoad: function () {
    var that = this
    that.getCrowdDetail();
    wx.showLoading({
      title: '加载中'
    })
  }
})
